import bz2
import csv
from pathlib import Path

from flask_restful import fields, marshal_with

from .biblr_rest import db, app


def get_verses():
    kjv_path = Path('kjv.tsv.bz2')
    with bz2.open(kjv_path, 'rt') as rfp:
        columns = ['book','chapter','verse','text']
        for row in csv.DictReader(rfp, columns, delimiter='\t'):
            yield row


def get_users():
    with open('users.csv') as rfp:
        columns = next(csv.reader(rfp))
        for row in csv.DictReader(rfp, columns):
            yield row


def populate_verses():
    if not Verse.query.first():
        app.logger.info('Populating verses')
        db.session.add_all(Verse(**v) for v in get_verses())
    else:
        app.logger.info('Verses already populated')


def populate_users():
    if not User.query.first():
        app.logger.info('Populating users')
        db.session.add_all(User(**u) for u in get_users())
    else:
        app.logger.info('Users already populated')


@app.cli.command('initdb')
def initdb_command():
    '''Initializes the database.'''
    db.create_all()
    populate_verses()
    populate_users()
    db.session.commit()
    app.logger.info('Initialized the database')


class MarshalMixin:
    @classmethod
    def marshal(cls, method):
        return marshal_with(cls.fields())(method)

    @staticmethod
    def fields():
        return {}


#----------------------------------------------------------------------
# Verse model
#
# Create a Verse model from the declarative base class "db.Model" and
# the MarshalMixin class above (i.e. use dual inheritance).
#
# Your model should have the following attributes:
#
# Columns
# - id (integer, primary key)
# - book (string)
# - chapter (integer)
# - verse (integer)
# - text (string)
#
# Relationships
# - comments (relationship with 'Comment' model, ordered by
#             'Comment.id', back popluates to 'verse')
#
# It should also have a @staticmethod called "fields" that returns a
# dictionary with the correct fields to marshal for this object
# (i.e. its columns)

class Verse(db.Model, MarshalMixin):
    id = db.Column(db.Integer, primary_key=True)
    book = db.Column(db.String, nullable=False)
    chapter = db.Column(db.Integer, nullable=False)
    verse = db.Column(db.Integer, nullable=False)
    text = db.Column(db.String, nullable=False)

    comments = db.relationship('Comment',order_by='Comment.id', back_populates='verse')
    @staticmethod
    def fields():
        return {
            'id': fields.Integer,
            'book': fields.String,
            'chapter': fields.Integer,
            'verse': fields.Integer,
            'text': fields.String,

    }

#----------------------------------------------------------------------
# User model
#
# Create a User model from the declarative base class "db.Model" and
# the MarshalMixin class above (i.e. use dual inheritance).
#
# Your model should have the following attributes:
#
# Columns
# - id (integer, primary key)
# - name (string)
# - phone (string)
# - email (string)
# - password (string)
#
# Relationships
# - comments (relationship with 'Comment' model, ordered by
#             'Comment.id', back popluates to 'user')
#
# It should also have a @staticmethod called "fields" that returns a
# dictionary with the correct fields to marshal for this object
# (i.e. its columns)

class User(db.Model, MarshalMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    phone = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False)
    password = db.Column(db.String, nullable=False)

    comments = db.relationship('Comment', order_by='Comment.id', back_populates='user')

    @staticmethod
    def fields():
        return {
            'id': fields.Integer,
            'name': fields.String,
            'phone': fields.String,
            'email': fields.String,
            'password': fields.String,
    }

#----------------------------------------------------------------------
# Comment model
#
# Create a Comment model from the declarative base class "db.Model" and
# the MarshalMixin class above (i.e. use dual inheritance).
#
# Your model should have the following attributes:
#
# Columns
# - id (integer, primary key)
# - text (string)
# - user_id (integer)
# - verse_id (integer)
#
# Relationships
# - user (relationship with 'User' model, back popluates to 'comments')
# - verse (relationship with 'Verse' model, back popluates to 'comments')
#
# It should also have a @staticmethod called "fields" that returns a
# dictionary with the correct fields to marshal for this object
# (i.e. its columns)

class Comment(db.Model, MarshalMixin):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    verse_id = db.Column(db.Integer, db.ForeignKey('verse.id'), nullable=False)

    user = db.relationship('User', back_populates='comments')
    verse = db.relationship('Verse', back_populates='comments')

    @staticmethod
    def fields():
        return {
            'id': fields.Integer,
            'text': fields.String,
            'user_id': fields.Integer,
            'verse_id': fields.Integer,
    }
